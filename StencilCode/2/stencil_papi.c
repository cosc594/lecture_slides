#include <stdio.h>
#include "papi.h"

#define TS 4
#define N 12
double A[N][N];

void print_matrix(double A[N][N]){
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            printf("%7.3lf ",A[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv){
    int counter=0;
    int ret_val;

    int event_names[] = {PAPI_L1_TCR, PAPI_L3_TCR};
    int event_set = PAPI_NULL;
    long long dt, cntr_val[2];

    for(int ti=0; ti<N; ti+=TS){
        for(int tj=0; tj<N; tj+=TS){
            for(int i=ti; i<ti+TS; i++){
                for(int j=tj; j<tj+TS; j++){
                    A[i][j] = 1.1*((i*i)%37);
                }
            }
        }
    }

    print_matrix(A);

    ret_val = PAPI_library_init(PAPI_VER_CURRENT);
    if (PAPI_VER_CURRENT != ret_val ){
        fprintf(stderr,"Error at PAPI_library_init()\n");
        return(-1);
    }

    ret_val = PAPI_create_eventset(&event_set);
    if (PAPI_OK != ret_val ){
        fprintf(stderr,"Error at PAPI_create_eventset()\n");
        return(-1);
    }

    ret_val = PAPI_add_events(event_set, event_names, 2);
    if (PAPI_OK != ret_val ){
        fprintf(stderr,"Error at PAPI_add_events()\n");
        return(-1);
    }

    ret_val = PAPI_start(event_set);
    if (PAPI_OK != ret_val ){
        fprintf(stderr,"Error at PAPI_start()\n");
        return(-1);
    }

    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            if( i-1>=0 && j-1>=0 && i+1<N && j+1<N )
                A[i][j] = (A[i][j]+A[i-1][j]+A[i+1][j]+A[i][j-1]+A[i][j+1])/5.0;
        }
    }

    ret_val = PAPI_stop(event_set, cntr_val);
    if (PAPI_OK != ret_val ){
        fprintf(stderr,"Error at PAPI_stop()\n");
        return(-1);
    }

    print_matrix(A);

    printf("L1:%lld L3:%lld\n", cntr_val[0], cntr_val[1]);

    return 0;
}
